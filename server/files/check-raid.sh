#!/bin/bash
# apt-get install mosquitto-clients
H=$(hostname -s)

function mqtt {
  mosquitto_pub -r -h k3s.oteixido.net -m "$2" -t "it/$H/$1"
}

function check_zpool_status {
  zpool status $1 | head -2 | tail -1 | sed 's/^ *state: //'
}

function check_zpool_scrub {
  zpool status $1 | head -3 | tail -1 | sed 's/^ *scan: //'
}

mqtt "disk/raid1/status" "$(check_zpool_status raid1)"
mqtt "disk/raid2/status" "$(check_zpool_status raid2)"
mqtt "disk/raid1/scrub" "$(check_zpool_scrub raid1)"
mqtt "disk/raid2/scrub" "$(check_zpool_scrub raid2)"

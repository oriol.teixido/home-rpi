#!/bin/bash -e

source ../base/common

function show_error_and_exit {
  echo "Arguments incorrectes."
  echo "     $0 install-disk"
  echo " "
  echo "     $0 install-server1"
  echo "     $0 install-server2"
  echo "     $0 sync-from-server1"
  echo "     $0 sync-to-server2"
  echo " "
  echo "     $0 sync-from-server2 --force"
  echo "     $0 sync-to-server1 --force"
  exit 1
}

function ssh_copy_base {
    ssh_execute "$1" "mkdir -p /root/docker/$BASE"
    ssh_copy "$1" "$BASE/$2" "/root/docker/$BASE/$3"
}

function ssh_execute_base {
    ssh_execute "$1" "mkdir -p /root/docker/$BASE"
    ssh_execute "$1" "cd /root/docker/$BASE && $2"
}

function ssh_copy_base_esh {
    ssh_execute "$1" "mkdir -p /root/docker/$BASE"
    ssh_copy_esh "$1" "$BASE/$2" "/root/docker/$BASE/$3"
}

function ssh_copy_esh {
    esh "$2.esh" > "$2.tmp"
    ssh_copy "$1" "$2.tmp" "$3"
    rm -f "$2.tmp"
}

function install-check-mqtt {
  HOSTNAME="$1"
  ssh_execute "${HOSTNAME}" "apt-get install -y mosquitto-clients"
  ssh_copy "${HOSTNAME}" "files/check-raid.sh" "/root/"
  add_crontab "${HOSTNAME}" "* * * * * /root/check-raid.sh"
}


function install-pihole {
  echo "No utilitzat actualment"
#  ssh_execute "$1" "apt-get install -y docker.io docker-compose"
#
#  ssh_execute "$1" "mkdir -p /root/docker/pihole"
#  GATEWAY="${NETWORK_GW}" DOMAIN="${NETWORK_DOMAIN}" HOSTNAME="$2" DHCP_START="$3" DHCP_END="$4" esh "files/docker-compose.pihole.yml.esh" > "files/docker-compose.pihole.yml"
#  ssh_copy "$1" "files/docker-compose.pihole.yml" "/root/docker/pihole/docker-compose.yml"
#  rm -f "files/docker-compose.pihole.yml"
#
#  SERVER1="${NETWORK_SERVER1_IP}" SERVER2="${NETWORK_SERVER2_IP}" esh "files/pihole/dnsmasq/99-second-DNS.conf.esh" > "files/99-second-DNS.conf"
#  ssh_copy "$1" "files/99-second-DNS.conf" "/root/docker/pihole/data/dnsmasq"
#  rm -f "files/99-second-DNS.conf"
#
#  ssh_execute "$1" "cd /root/docker/pihole && docker-compose down"
#  ssh_execute "$1" "cd /root/docker/pihole && docker-compose up -d"
}

function install-nut {
  ssh_execute "$1" "sudo apt-get install -y nut"

  ssh_copy "$1" "nut/nut.conf" "/etc/nut/nut.conf"
  ssh_copy "$1" "nut/ups.conf" "/etc/nut/ups.conf"
  ssh_copy "$1" "nut/upsd.conf" "/etc/nut/upsd.conf"
  ssh_copy_esh "$1" "nut/upsd.users" "/etc/nut/upsd.users"
  ssh_copy_esh "$1" "nut/upsmon.conf" "/etc/nut/upsmon.conf"
  ssh_copy "$1" "nut/upssched.conf" "/etc/nut/upssched.conf"
}

function install-torrent {
  BASE="torrent"
  ssh_execute "$1" "apt-get install -y docker.io docker-compose"
  ssh_copy_base_esh "$1" "docker-compose.yml" "docker-compose.yml"
  ssh_copy_base "$1" "ch-nl-01.protonvpn.net.udp.ovpn" "transmission/vpn-configs-contrib/openvpn/protonvpn/ch-nl-01.protonvpn.net.udp.ovpn"
}

function install-wireguard {
  BASE="wireguard"
  ssh_execute "$1" "apt-get install -y docker.io docker-compose"
  SERVERPORT="$2" SUBNET="$3" SERVERURL="$4" ssh_copy_base_esh "$1" "docker-compose.yml" "docker-compose.yml"
  ssh_copy_base "$1" "generate-peer.sh" "generate-peer.sh"
  ssh_execute_base "$1" "docker-compose down"
  ssh_execute_base "$1" "docker-compose up -d"
}

function install-dhcpd {
  BASE="dhcpd"
  ssh_execute "$1" "apt-get install -y docker.io docker-compose"

  ssh_execute_base "$1" "mkdir -p var"
  ssh_execute_base "$1" "touch var/dhcpd.leases"
  ssh_copy_base "$1" "docker-compose.yml" "docker-compose.yml"
  DHCP_START="$2" DHCP_END="$3" ssh_copy_base_esh "$1" "dhcpd.conf" "dhcpd.conf"
  ssh_execute_base "$1" "docker-compose down"
  ssh_execute_base "$1" "docker-compose up -d"
}

function install-dns {
  BASE="dns"
  ssh_execute "$1" "apt-get install -y docker.io docker-compose"

  HOSTNAME="$2" ssh_copy_base_esh "$1" "docker-compose.yml" "docker-compose.yml"
  ssh_copy_base_esh "$1" "dnsmasq.conf" "dnsmasq.conf"
  ssh_copy_base_esh "$1" "hosts" "hosts"
  ssh_copy_base "$1" "nginx.conf" "nginx.conf"
  ssh_execute_base "$1" "docker-compose down"
  ssh_execute_base "$1" "docker-compose up -d"
}

function sync-from-server {
  mkdir -p ./data/$2
  sudo rsync --archive --delete --verbose root@$1:/root/docker/$2/data/ ./data/$2/
  ssh_execute $1 "cd /root/docker/$2 && docker-compose down"
  sudo rsync --archive --delete --verbose root@$1:/root/docker/$2/data/ ./data/$2/
  ssh_execute $1 "cd /root/docker/$2 && docker-compose up -d"
}

function sync-to-server {
  BASE="$2"
  ssh_execute_base "$1" "docker-compose down"
  sudo rsync --archive --delete --verbose ./data/$BASE/ root@"$1":/root/docker/$BASE/data/
  if [ "$2" == "wireguard" ]; then
    ssh_copy_base "$1" "replace-net.sh" "replace-net.sh"
    ssh_execute_base $1 "./replace-net.sh"
  fi
  ssh_execute $1 "cd /root/docker/$2 && docker-compose up -d"
}

function start-server {
  ssh_execute $1 "cd /root/docker/$2 && docker-compose up -d"
}

function stop-server {
  ssh_execute $1 "cd /root/docker/$2 && docker-compose down"
}

if [ "$#" -lt "1" ]; then
  show_error_and_exit
fi
ACTION="$1"

case "$ACTION" in
  "install-disk")
    HOSTNAME="disk.oteixido.net"
    install-check-mqtt "${HOSTNAME}"
    add_crontab "${HOSTNAME}" "@reboot hdparm --yes-i-know-what-i-am-doing -s 1 -S 120 /dev/disk/by-id/ata-ST2000DM008-2FR102_WFL14L2Z"
    add_crontab "${HOSTNAME}" "@reboot hdparm --yes-i-know-what-i-am-doing -s 1 -S 120 /dev/disk/by-id/ata-ST2000DM006-2DM164_Z8E0RMAN"
  ;;
  "install-server1")
    install-nut        "${NETWORK_SERVER1_IP}"
    install-wireguard  "${NETWORK_SERVER1_IP}" "51820" "10.13.13.0" "home1.oteixido.net"
    install-dhcpd      "${NETWORK_SERVER1_IP}" "${NETWORK_SERVER1_DHCP_START}" "${NETWORK_SERVER1_DHCP_END}"
    install-dns        "${NETWORK_SERVER1_IP}" "server1.oteixido.net"
  ;;
  "install-server2")
    install-wireguard  "${NETWORK_SERVER2_IP}" "51821" "20.13.13.0" "home2.oteixido.net"
    install-dhcpd      "${NETWORK_SERVER2_IP}" "${NETWORK_SERVER2_DHCP_START}" "${NETWORK_SERVER2_DHCP_END}"
    install-dns        "${NETWORK_SERVER2_IP}" "server2.oteixido.net"
    install-torrent    "${NETWORK_SERVER2_IP}"
  ;;
  "sync-from-server1")
    sync-from-server "${NETWORK_SERVER1_IP}" wireguard
    sync-from-server "${NETWORK_SERVER1_IP}" dns
  ;;
  "sync-to-server2")
    sync-to-server "${NETWORK_SERVER2_IP}" wireguard
    sync-to-server "${NETWORK_SERVER1_IP}" dns
  ;;
  "sync-from-server2")
    if [ "$2" != "--force" ]; then 
      show_error_and_exit
    fi
    sync-from-server "${NETWORK_SERVER2_IP}" wireguard
    sync-from-server "${NETWORK_SERVER2_IP}" dns
  ;;
  "sync-to-server1")
    if [ "$2" != "--force" ]; then 
      show_error_and_exit
    fi
    sync-to-server "${NETWORK_SERVER1_IP}" wireguard
    sync-to-server "${NETWORK_SERVER1_IP}" dns
  ;;
  *)
    show_error_and_exit
  ;;
esac
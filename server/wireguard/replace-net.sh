#!/bin/bash
find data -type f -name "*.conf" -print0 | xargs -0 sed --in-place 's/home1\.oteixido\.net/home2\.oteixido\.net/g'
find data -type f -name "*.conf" -print0 | xargs -0 sed --in-place 's/10\.13\.13\./20\.13\.13\./g'
find data -type f -name "*.conf" -print0 | xargs -0 sed --in-place 's/51820/51821/g'
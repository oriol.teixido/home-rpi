#!/bin/bash
export NAME=$1
export IP=$2
pushd /root/docker/wireguard/data
if [ -d "peer_$NAME" ]; then
  echo "Atenció. Ja existeix la confinguració del peer $NAME."
  exit 1
fi
mkdir -p peer_$NAME
docker exec -i wireguard wg genkey > peer_$NAME/privatekey-peer_$NAME
cat peer_$NAME/privatekey-peer_$NAME | docker exec -i wireguard wg pubkey > peer_$NAME/publickey-peer_$NAME
echo "[Interface]" > peer_$NAME/peer_$NAME.conf
echo "Address = $IP" >> peer_$NAME/peer_$NAME.conf
echo "PrivateKey = $(cat peer_$NAME/privatekey-peer_$NAME)" >> peer_$NAME/peer_$NAME.conf
echo "ListenPort = 51820" >> peer_$NAME/peer_$NAME.conf
echo "[Peer]" >> peer_$NAME/peer_$NAME.conf
echo "PublicKey = MhBCHRC3+SqJ00BTTbzbVk4uUsuThbVWVtCkc25pvlo=" >> peer_$NAME/peer_$NAME.conf
echo "Endpoint = home1.oteixido.net:51820" >> peer_$NAME/peer_$NAME.conf
echo "AllowedIPs = 10.13.13.0/24" >> peer_$NAME/peer_$NAME.conf
echo "PersistentKeepalive = 25" >> peer_$NAME/peer_$NAME.conf
chown pi:pi -R peer_$NAME
chmod go-wrx -R peer_$NAME
# Instal·lació base

## Instal·lació a ubuntu-server

Configurar de xarxa

```console
$ cat /etc/netplan/00-installer-config.yaml 
network:
  ethernets:
    enp1s0:
      addresses:
      - 192.168.0.XX/24
      nameservers:
        addresses:
        - 192.168.0.2
        - 192.168.0.3
        search:
        - oteixido.net
      routes:
      - to: default
        via: 192.168.0.1
  version: 2
```

Copiar les claus *authorized_keys* a l'usuari root.

## Crear la connexió de wireguard

En cas de no existir cal afegir les claus de la connexió a wireguard. 

## Configurar el servidor.

```bash
./config.sh <server1|server2|node1|...>
```
#!/bin/bash -e

source ../common

if [ "$#" != "2"  ] && [ "$#" != "3" ]; then
    echo "Arguments incorrectes."
    echo "   $0 <name> <ip> [dns_server]"
    echo "   ex. $0 server1 192.168.0.2 1.1.1.1"
    echo "   ex. $0 node3 192.168.0.13"
    exit 1
fi

export HOST_NAME="$1"
export HOST_IP="$2"
export HOST_DNS="$3"

export ROOT_FS="/media/${SUDO_USER}/rootfs"
export BOOT_FS="/media/${SUDO_USER}/bootfs"

function sd-check {
  required BOOT_FS
  required ROOT_FS

  if [ "$EUID" -ne "0" ]; then
      echo "Cal executar com a root"
      exit 1
  fi

  if [ ! -d "${BOOT_FS}" ]; then
      echo "\"${BOOT_FS}\" no és un directori."
      exit 1
  fi

  if [ ! -d "${ROOT_FS}" ]; then
      echo "\"${ROOT_FS}\" no és un directori."
      exit 1
  fi
}

function sd-set-ssh {
  required BOOT_FS
  required ROOT_FS

  empty "${BOOT_FS}/ssh"
  mkdir -p "${ROOT_FS}/root/.ssh"
  cp "${DIR}/files/authorized_keys" "${ROOT_FS}/root/.ssh/authorized_keys"
  chmod 600 "${ROOT_FS}/root/.ssh/authorized_keys"

  write "${BOOT_FS}/userconf.txt" "pi:*"
}

function sd-set-network {
  required ROOT_FS
  required HOST_NAME
  required HOST_IP
  required NETWORK_GW
  required NETWORK_DNS
  required NETWORK_DOMAIN

  empty "${ROOT_FS}/etc/network/interfaces.d/eth0"
  write "${ROOT_FS}/etc/network/interfaces.d/eth0" "auto eth0"
  write "${ROOT_FS}/etc/network/interfaces.d/eth0" "iface eth0 inet static"
  write "${ROOT_FS}/etc/network/interfaces.d/eth0" "  address ${HOST_IP}/24"
  write "${ROOT_FS}/etc/network/interfaces.d/eth0" "  gateway ${NETWORK_GW}"
  if [ -z "${HOST_DNS}" ]; then
    write "${ROOT_FS}/etc/network/interfaces.d/eth0" "  dns-nameservers ${NETWORK_DNS}"
  else
    write "${ROOT_FS}/etc/network/interfaces.d/eth0" "  dns-nameservers ${HOST_DNS}"
  fi

  empty "${ROOT_FS}/etc/hostname"
  write "${ROOT_FS}/etc/hostname" "${HOST_NAME}.${NETWORK_DOMAIN}"

  write-if-not-exists "${ROOT_FS}/etc/hosts" "${HOST_IP} ${HOST_NAME}.${NETWORK_DOMAIN}"
}

function sd-set-64bits {
  required BOOT_FS

  write "${BOOT_FS}/config.txt" "arm_64bit=1"
}

function sd-set-cgroup {
  required ROOT_FS

  # Necessari per k3s
  sed -i -e 's/$/ cgroup_memory=1 cgroup_enable=memory/' "${BOOT_FS}/cmdline.txt"
}

sd-check
sd-set-ssh
sd-set-network
sd-set-64bits
sd-set-cgroup

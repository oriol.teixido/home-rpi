# Preparar la targeta SD

Baixar i instal·lar [Raspbian GNU/Linux Lite 64bits](https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2022-09-26/2022-09-22-raspios-bullseye-arm64-lite.img.xz).


```bash
sudo apt install rpi-imager
export DEV_IMG=/dev/sdb
export RPI_IMG=2022-09-22-raspios-bullseye-arm64-lite.img
sudo rpi-imager --cli "${RPI_IMG}" "${DEV_IMG}"
sudo sync
````

Desmuntar la SD i tornar-la a connectar.

```bash
sudo ./sdcard.sh server1 192.168.0.2 1.1.1.1
sudo ./sdcard.sh node3 192.168.0.13
```

Connectar a la xarxa i ampliar l'espai de la partició a l'espai de la targeta SD.

```bash
PWD=$(pass my/home/user.pi.password)
ssh root@$NODE_IP "echo -e '${PWD}\n${PWD}' | passwd pi"
ssh root@$NODE_IP "raspi-config --expand-rootfs"
ssh root@$NODE_IP "reboot"
```
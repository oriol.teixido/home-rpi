#!/bin/bash

function show_error {
  echo "Arguments incorrectes."
  echo "     $0 <node3|node4|node5|server1|server2|...>"
  exit 1
}

function config_base {
  NAME=$1
  HOSTNAME="${NAME}.${NETWORK_DOMAIN}"
  ssh_execute "${HOSTNAME}" "apt-get update"
  ssh_execute "${HOSTNAME}" "DEBIAN_FRONTEND=noninteractive apt-get install locales cron"
  ssh_execute "${HOSTNAME}" "locale-gen en_GB.UTF-8 es_ES.UTF-8"
  ssh_execute "${HOSTNAME}" "DEBIAN_FRONTEND=noninteractive apt-get full-upgrade -y"
  ssh_execute "${HOSTNAME}" "DEBIAN_FRONTEND=noninteractive apt-get autoremove -y"
  ssh_execute "${HOSTNAME}" "ln -fs /usr/share/zoneinfo/Europe/Madrid /etc/localtime"
  ssh_execute "${HOSTNAME}" "dpkg-reconfigure -f noninteractive tzdata"
  ssh_copy    "${HOSTNAME}" "files/update-authorized-keys" "/etc/cron.daily/update-authorized-keys"
  ssh_execute "${HOSTNAME}" "service cron restart"
}

function config_wireguard {
  NAME=$1
  HOSTNAME="${NAME}.${NETWORK_DOMAIN}"

  FILE="../server/data/wireguard/peer_${NAME}/peer_${NAME}.conf"
  if [ ! -f "$FILE" ]; then
    echo "No existeix el fitxer '$FILE'."
    exit 1
  fi
  ssh_execute "${HOSTNAME}" "DEBIAN_FRONTEND=noninteractive apt-get install -y wireguard"
  ssh_copy    "${HOSTNAME}" "$FILE" "/etc/wireguard/wg0.conf"
  ssh_execute "${HOSTNAME}" "chmod 600 /etc/wireguard/wg0.conf"
  ssh_execute "${HOSTNAME}" "wg-quick up wg0"
  ssh_copy    "${HOSTNAME}" "files/check-wg0" "/etc/cron.hourly/check-wg0"

  ssh_copy    "${HOSTNAME}" "$FILE" "/etc/wireguard/wg1.conf"
  ssh_execute "${HOSTNAME}" "sed --in-place 's/10\.13\.13\./20\.13\.13\./g' /etc/wireguard/wg1.conf"
  ssh_execute "${HOSTNAME}" "sed --in-place 's/51820/51821/g' /etc/wireguard/wg1.conf"
  ssh_execute "${HOSTNAME}" "chmod 600 /etc/wireguard/wg1.conf"
  ssh_execute "${HOSTNAME}" "wg-quick up wg1"
  ssh_copy    "${HOSTNAME}" "files/check-wg1" "/etc/cron.hourly/check-wg1"

  ssh_execute "${HOSTNAME}" "service cron restart"
  add_crontab "${HOSTNAME}" "@reboot /etc/cron.hourly/check-wg0"
  add_crontab "${HOSTNAME}" "@reboot /etc/cron.hourly/check-wg1"
}

function config_sensors {
    NAME=$1
    HOSTNAME="${NAME}.${NETWORK_DOMAIN}"

    ssh_execute "${HOSTNAME}" "id sensors || adduser sensors --system"
    ssh_execute "${HOSTNAME}" "DEBIAN_FRONTEND=noninteractive apt-get install -y git python3-dev python3-apt python3-pip"
    ssh_execute "${HOSTNAME}" "rm -rf /home/sensors/system_sensors && cd /home/sensors && git clone https://github.com/Sennevds/system_sensors.git"
    ssh_execute "${HOSTNAME}" "cd /home/sensors/system_sensors && pip3 install -r requirements.txt"
    export SERVER="${NETWORK_K3S_IP}" NAME="${NAME}" && cat files/system_sensors.yaml | envsubst '$NAME $SERVER' > "/tmp/settings.${NAME}.yaml"
    ssh_copy    "${HOSTNAME}" "/tmp/settings.${NAME}.yaml" "/home/sensors/system_sensors/src/settings.yaml"
    rm -f "/tmp/settings.${NAME}.yaml"
    ssh_execute "${HOSTNAME}" "chown -R sensors:nogroup /home/sensors/system_sensors"
    ssh_copy    "${HOSTNAME}" "files/system_sensors.service" "/etc/systemd/system/system.sensors.service"
    ssh_execute "${HOSTNAME}" "systemctl daemon-reload"
    ssh_execute "${HOSTNAME}" "systemctl enable --now system.sensors.service"
}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
pushd "$SCRIPT_DIR" || exit 1

source "./common"
if [ "$#" -ne "1" ]; then
  show_error
fi

config_base "$1"
config_wireguard "$1"
config_sensors "$1"

popd
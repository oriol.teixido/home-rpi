#!/bin/bash

# UBUNTU 24.04.01

source ../base/common

function show_error_and_exit {
  echo "Arguments incorrectes."
  echo "     $0 [install|upgrade|config] <HOSTNAME>"
  exit 1
}

function create-link-cloud {
  USERNAME="$1"
  CLOUD="$2"
  FOLDER="$3"
  if [ "$USERNAME" == "" ] || [ "$CLOUD" == "" ]; then
    echo "Errors als paràmetres d'entrada de la funció"
    exit 1
  fi

  if [ "$FOLDER" == "" ]; then
    FOLDER="$CLOUD"
  fi
  PARENT=$(dirname "/home/$USERNAME/$FOLDER")
  ssh_my_execute "mkdir -p '$PARENT' && chown $USERNAME:$USERNAME --no-dereference '$PARENT'"
  ssh_my_execute "rm -rf '/home/$USERNAME/$FOLDER'"
  ssh_my_execute "ln -s '/home/$USERNAME/Nextcloud/Laptop/$CLOUD/' '/home/$USERNAME/$FOLDER'"
  ssh_my_execute "chown $USERNAME:$USERNAME --no-dereference '/home/$USERNAME/$FOLDER'"
}

function upgrade {
  echo "=====> Upgrading $REMOTE..."
  ssh_my_execute "apt-get update && apt-get upgrade -y && apt-get auto-remove -y"
  ssh_my_execute "snap refresh"
}

function install_base {
  echo "=====> Installing base on $REMOTE..."
  ssh_my_apt_install tilix openssh-server apt-transport-https gnupg2 curl ecryptfs-utils nextcloud-desktop
  ssh_my_apt_install cifs-utils cups smbclient python3-smbc net-tools cryfs pass
  ssh_my_apt_install gnome-tweaks gnome-shell-extensions
  ssh_my_apt_install calibre pdftk-java
  ssh_my_execute "echo virtualbox-ext-pack virtualbox-ext-pack/license select true | debconf-set-selections"
  ssh_my_apt_install virtualbox virtualbox-ext-pack

  ssh_my_snap_install firefox
  ssh_my_snap_install thunderbird
  ssh_my_snap_install chromium
  ssh_my_snap_install spotify
  ssh_my_snap_install bitwarden
  ssh_my_snap_install joplin-desktop
  ssh_my_snap_install superproductivity

  ssh_my_copy_and_install "megasync-xUbuntu_24.04_amd64.deb" "Download https://mega.io/es/desktop#download"

  # VPN Proton https://protonvpn.com/support/official-ubuntu-vpn-setup/
  ssh_my_copy_and_install "protonvpn-stable-release_1.0.4_all.deb" "Download  https://repo.protonvpn.com/debian/dists/stable/main/binary-all/protonvpn-stable-release_1.0.4_all.deb"
  ssh_my_execute "apt-get update"
  ssh_my_apt_install proton-vpn-gnome-desktop
}

function install_multimedia {
  echo "=====> Installing multimedia on $REMOTE..."
  ssh_my_apt_install vlc ffmpeg sane-utils imagemagick gimp handbrake
  ssh_my_snap_install spotify
}

function install_games {
  echo "=====> Installing games on $REMOTE..."
  ssh_my_apt_install openarena
  ssh_my_snap_install 0ad
  ssh_my_snap_install openra
  ssh_my_snap_install ultrastar-worldparty
  ssh_my_copy_and_install "newcp_1.6.1_amd64.deb" "Download https://newcp.net/download" # New club penguin
}

function install_certificates {
  echo "=====> Installing CERTIFICATES on $REMOTE..."
  ssh_my_apt_install openjdk-21-jre libnss3-tools
  ssh_my_copy_and_install "AutoFirma_1_8_3.deb" "Informació a https://firmaelectronica.gob.es/ i a https://firmaelectronica.gob.es/Home/Descargas.html"
}

function install_upc {
  echo "=====> Installing UPC on $REMOTE..."
  ssh_my_apt_install remmina linphone-desktop
  ssh_my_apt_install libgconf-2-4 libnss3-tools
  ssh_my_copy_and_install "forticlient_vpn_7.4.0.1636_amd64.deb" "Descàrrega a https://serveistic.upc.edu/ca/upclink/software-upclink/ubuntu/forticlient_vpn_7-0-7-0246_amd64.deb"
}

function install_devel {
  echo "=====> Installing devel  $REMOTE..."

  # Python
  ssh_my_apt_install python3 python3-pip python3-venv

  # Docker
  ssh_my_apt_install docker.io docker-compose

  # Kubernetes
  ssh_my_execute "curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.31/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg"
  ssh_my_execute "echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.31/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list"
  ssh_my_execute "apt-get update && apt-get install -y kubectl"

  # Utilitats
   ssh_my_apt_install git git-extras meld shellcheck
   ssh_my_snap_install dbeaver-ce
   ssh_my_snap_install postman
   ssh_my_snap_install sublime-text --classic

  # gitlab-ci-local
   ssh_my_execute "wget -O /etc/apt/sources.list.d/gitlab-ci-local.sources https://gitlab-ci-local-ppa.firecow.dk/gitlab-ci-local.sources"
   ssh_my_execute "apt-get update"
   ssh_my_execute "apt-get install gitlab-ci-local"
}

function install_devel_games {
  # Unity Hub https://docs.unity3d.com/hub/manual/InstallHub.html
  ssh_my_execute "curl -fsSL https://hub.unity3d.com/linux/keys/public | gpg --dearmor -o /etc/apt/keyrings/Unity_Technologies_ApS.gpg"
  ssh_my_execute "echo 'deb [signed-by=/etc/apt/keyrings/Unity_Technologies_ApS.gpg] https://hub.unity3d.com/linux/repos/deb stable main' | tee /etc/apt/sources.list.d/unityhub.list"
  ssh_my_execute "apt-get update && apt-get install -y unityhub"
}

function config_wifi {
  wifis=( home simon sonia berta musica )
  for wifi in "${wifis[@]}"
  do
    export WIFI_SSID="$(pass my/home/laptop/wifi.${wifi}.ssid)"
    export WIFI_KEY="$(pass my/home/laptop/wifi.${wifi}.password)"
    export WIFI_UUID="$(uuidgen)"
    ssh_my_copy_esh "files/wifi.nmconnection" "/etc/NetworkManager/system-connections/${WIFI_SSID}.nmconnection"
  done
  ssh_my_execute "chmod 600 /etc/NetworkManager/system-connections/*.nmconnection"
  ssh_my_execute "systemctl restart NetworkManager"
}

function create_user {
  USERNAME=$1
  USERID=$2
  PASSWORD=$(pass my/home/laptop/user.$USERNAME.password)
  ssh_my_execute "useradd --uid $USERID --home-dir /home/$USERNAME --create-home --shell /bin/bash --comment ${USERNAME^} $USERNAME"
  ssh_my_execute "echo -e \"$PASSWORD\n$PASSWORD\" | passwd $USERNAME"
  create-link-cloud "$USERNAME" "bin"
  ssh_my_execute "chmod u+x /home/$USERNAME/bin/*.sh"
  ssh_my_execute_as_user "${USERNAME}" "xdg-user-dirs-update --force"
  create-link-cloud "$USERNAME" "Desktop" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir DESKTOP)')"
  create-link-cloud "$USERNAME" "Music" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir MUSIC)')"
  create-link-cloud "$USERNAME" "Documents" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir DOCUMENTS)')"
  create-link-cloud "$USERNAME" "Pictures" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir PICTURES)')"
  create-link-cloud "$USERNAME" "Templates" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir TEMPLATES)')"
  create-link-cloud "$USERNAME" "Videos" "$(ssh_my_execute_as_user "${USERNAME}" 'basename $(xdg-user-dir VIDEOS)')"
}

function ssh_my_copy_and_install {
  if [ ! -f "files/$1" ]; then
    echo "No s'ha trobat el fitxer files/$1"
    if [ -n "$2" ]; then
      echo "=====> $2"
    fi
    exit 1
  fi
  ssh_my_execute "mkdir -p ~/install/"
  ssh_my_copy "files/$1" "~/install/$1"
  ssh_my_apt_install "-f ~/install/$1"
}

function config_user_ext {
  ssh_my_execute "adduser $1 sudo"
  ssh_my_execute "adduser $1 vboxusers"
  ssh_my_execute "addgroup --system docker && adduser $1 docker"
  create-link-cloud "$1" ".config/remmina"
  create-link-cloud "$1" ".local/share/remmina"
  create-link-cloud "$1" ".local/share/applications"
  create-link-cloud "$1" "Biblioteca de calibre"
}

if [ "$#" -lt "2" ]; then
  show_error_and_exit
fi
ACTION="$1"
NAME="$2"

REMOTE="$NAME.${NETWORK_DOMAIN}"

case "$ACTION" in
  "upgrade")
    upgrade
  ;;
  "install")
    install_base
    install_multimedia
    install_games
    install_certificates
    install_upc
    install_devel
    install_devel_games
  ;;
  "config")
#    config_wifi
#    create_user oriol 1000
    create_user patricia 1001
    create_user samuel 1002
    create_user laura 1003
    create_user guest 1004
#    config_user_ext oriol
    config_user_ext patricia
  ;;
  *)
    show_error_and_exit
  ;;
esac

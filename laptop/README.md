# Ubuntu 22.04 (Català)

## Instal·lar software

```bash
export NAME="<pc0|pc1|pc2>"
../base/config.sh "$NAME"
./config.sh install $NAME
./config.sh config $NAME
```

## Configurar software 

### zsh

```bash
sudo apt-get install zsh
chsh -s $(which zsh) --> Canviar per defecte
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

### VPN Proton

https://protonvpn.com/support/official-ubuntu-vpn-setup/

```bash
sudo dpkg --install protonvpn-stable-release_1.0.3-2_all.deb
sudo apt install libayatana-appindicator3-1 gir1.2-ayatanaappindicator3-0.1 gnome-shell-extension-appindicator
sudo apt-get update
sudo apt-get install proton-vpn-gnome-desktop
```

## Software no instal·lat

## Authenticator

```bash
sudo apt-get install flatpak
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.github.bilelmoussaoui.Authenticator
flatpak run com.github.bilelmoussaoui.Authenticator
```

## Obs

```bash
sudo apt-get install ffmpeg
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt-get update && sudo apt-get install obs-studio
wget https://github.com/Palakis/obs-ndi/releases/download/4.9.1/libndi4_4.5.1-1_amd64.deb
sudo apt-get install -f ./libndi4_4.5.1-1_amd64.deb
wget https://github.com/Palakis/obs-ndi/releases/download/4.9.1/obs-ndi_4.9.1-1_amd64.deb
sudo apt-get install -f ./obs-ndi_4.9.1-1_amd64.deb
```

## Chrome
Baixar-se [Google Chrome](https://www.google.com/chrome/)

```bash
sudo apt-get install -f ./google-chrome-stable_current_amd64.deb
```

## Visual Studio

Baixar el *.deb del l'adreça https://code.visualstudio.com/docs/setup/linux

```bash
sudo apt-get install -f ./<file.deb>
```

## Software utilitzat puntualment

* **testdisk (photorec)**: Programa per recuperar fotos esborrades.
* **picard**: Programa per organitzar i etiquetar fitxers mp3.
* **fslint**: Programa per buscar imatges duplicades.
* **pinta**: Programa de dibuix.
* **qrencode**: Programa per generar codis QR
* **nagstamon**:
* **virt-viewer**:

# Drivers bluetooth (ASUS X554L)

El firmware es pot trobar al repositori [https://github.com/winterheart/broadcom-bt-firmware].

```bash
sudo cp firmware/BCM43142A0-04ca-2006.hcd /lib/firmware/brcm/
sudo chown root:root /lib/firmware/brcm/*.hcd
sudo apt-get install pulseaudio-module-bluetooth
```

No funciona el wifi de la placa integrada si s'utilitzar el bluetooth de la mateixa placa. Per tant, cal desactivar el dispositiu.

```bash
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="04ca", ATTRS{idProduct}=="2006", ATTR{authorized}="0"' | sudo tee /etc/udev/rules.d/81-bluetooth-hci.rules
```





ssh -X root@pc1.oteixido.net

add-apt-repository ppa:mjasnik/ppa
apt update
apt install timekpr-next

timekpra

sudo apt-get remove --purge timekpr-next
sudo add-apt-repository -r ppa:mjasnik/ppa









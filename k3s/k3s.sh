#!/bin/bash

source ../base/common
source ./cluster

case "$1" in
  install)
    pushd system || exit 1;   ./k3s.sh install; popd || exit 1
    pushd services || exit 1; ./k3s.sh install; popd || exit 1
  ;;
  uninstall)
    pushd services || exit 1; ./k3s.sh uninstall; popd || exit 1
    pushd system || exit 1;   ./k3s.sh uninstall; popd || exit 1
  ;;
  set-labels)
    # hardware/renderD128
    # hardware/disk
    # hardware/download
    # apps/stable
    for NODE in "${NODES[@]}";
    do
        kubectl label nodes ${NODE} hardware/renderD128=false --overwrite
        kubectl label nodes ${NODE} hardware/disk=false --overwrite
        kubectl label nodes ${NODE} hardware/download=false --overwrite
        kubectl label nodes ${NODE} apps/stable=false --overwrite
    done    
    for NODE in "${NODE_RENDER[@]}";
    do
        kubectl label nodes "${NODE}" hardware/renderD128=true --overwrite
    done    
    for NODE in "${NODE_STABLE[@]}";
    do
        kubectl label nodes "${NODE}" apps/stable=true --overwrite
    done
    if [ -n "${NODE_DISK}" ]; then
      kubectl label nodes "${NODE_DISK}" hardware/disk=true --overwrite
    fi
    if [ -n "${NODE_DOWNLOAD}" ]; then
      kubectl label nodes "${NODE_DOWNLOAD}" hardware/download=true --overwrite
    fi
    kubectl patch deploy coredns -n kube-system -p '{"spec": {"template": {"spec": {"nodeSelector": {"apps/stable": "true"}}}}}'
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|set-labels|ssl-update]"
    exit 1
  ;;
esac

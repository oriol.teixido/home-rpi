#!/bin/bash

source ../../../base/common
source ../../cluster

# https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/
# wget -O recommended.yml -N https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml

function install {
  kubectl apply -f recommended.yml
  kubectl apply -f admin-user.yml
  kubectl apply -f ingress.yml
  kubectl get secret admin-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d | pass insert -f -e my/home/k3s/dashboard.token
}

function uninstall {
  kubectl delete -f ingress.yml
  kubectl delete -f recommended.yml
}

function download {
  GITHUB_URL=https://github.com/kubernetes/dashboard/releases
  curl -o recommended.yml https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;    
  download)
    download
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall|download]"
    exit 1
  ;;
esac

#!/bin/bash

source ../../../base/common
source ../../cluster

# https://kubernetes.github.io/ingress-nginx/deploy/
# wget -O deploy.yml -N https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.4.0/deploy/static/provider/cloud/deploy.yaml
# wget -O deploy.yml -N https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/baremetal/deploy.yaml

case "$1" in
  install)
    kubectl apply -f deploy.yml
    esh secrets.yml.esh | kubectl apply -f -
    kubectl patch deploy ingress-nginx-controller -n ingress-nginx --type="json" -p='[{"op":"add","path":"/spec/template/spec/containers/0/args/-","value":"--default-ssl-certificate=ingress-nginx/tls-ingress"}]'
    kubectl patch service ingress-nginx-controller -n ingress-nginx -p "{\"spec\": {\"externalIPs\": [\"$NODE_MASTER_IP\"]}}"
  ;;
  ssl-create)
    docker run -it --rm --name certbot \
      -v "${PWD}/runtime:/etc/letsencrypt" \
      -v "${PWD}/cdmon-dns-auth.sh:/bin/cdmon-dns-auth.sh" \
      -e "CDMON_APIKEY=$(pass my/home/cdmon.apikey)" \
      certbot/certbot certonly --manual --email "${EMAIL}" --manual-auth-hook cdmon-dns-auth.sh --preferred-challenges dns --debug-challenges -d "*.${NETWORK_DOMAIN}"
    sudo chown -R $(id --user):$(id --group) "${PWD}/runtime"
    pass-create-from-file my/home/k3s/${NETWORK_DOMAIN}.crt "${PWD}/runtime/live/${NETWORK_DOMAIN}/fullchain.pem"
    pass-create-from-file my/home/k3s/${NETWORK_DOMAIN}.key "${PWD}/runtime/live/${NETWORK_DOMAIN}/privkey.pem"
    rm -rf "${PWD}/runtime"
  ;;
  ssl-update)
    esh secrets.yml.esh | kubectl apply -f -
    kubectl rollout restart deployment --namespace ingress-nginx ingress-nginx-controller
  ;;
  uninstall)
    kubectl delete -f deploy.yml
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|ssl-create|ssl-update]"
    exit 1
  ;;
esac

#!/bin/sh
apk add curl
curl -s -S --include \
     --request POST \
     --header "Content-Type: application/json" \
     --header "Accept: application/json" \
     --header "apikey: ${CDMON_APIKEY}" \
     --data-binary "{\"data\": {\"domain\": \"${CERTBOT_DOMAIN}\", \"current\": {\"host\": \"_acme-challenge\", \"type\": \"TXT\"}, \"new\": {\"ttl\": 900, \"value\": \"${CERTBOT_VALIDATION}\"} } }" \
'https://api-domains.cdmon.services/api-domains/dnsrecords/edit'
sleep 60
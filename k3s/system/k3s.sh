#!/bin/bash

function install {
  pushd "$1"
  ./k3s.sh install
  popd
}

function uninstall {
  pushd "$1"
  ./k3s.sh uninstall
  popd
}

case "$1" in
  install)
    install ingress
    install dashboard
  ;;
  uninstall)
    uninstall dashboard
    uninstall ingress
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall]"
    exit 1
  ;;
esac

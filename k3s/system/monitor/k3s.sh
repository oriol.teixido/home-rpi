#!/bin/bash

source ../../../base/common
source ../../cluster

case "$1" in
  install)
    kubectl apply -f namespace.yml
    esh secrets.yml.esh | kubectl apply -f -
    for NODE in "${NODES[@]}";
    do
      export NODE=$(echo "$NODE" | cut -d. -f1)
      esh services.yml.esh | kubectl apply -f -
      esh ingress.yml.esh | kubectl apply -f -
    done    
  ;;
  uninstall)
    kubectl delete -f namespace.yml
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall]"
    exit 1
  ;;
esac

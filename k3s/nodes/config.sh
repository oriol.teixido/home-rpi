#!/bin/bash -e

source ../../base/common
source ../cluster

function show_error {
  echo "Arguments incorrectes."
  echo "     $0 <acció> <name>"
  echo ""
  echo "Accions:"
  echo "     - k3s-node-delete"
  echo " "
  echo "     - k3s-master"
  echo "     - k3s-master-upgrade"
  echo "     - k3s-master-uninstall"
  echo " "
  echo "     - k3s-worker"
  echo "     - k3s-worker-upgrade"
  echo "     - k3s-worker-uninstall"
  echo " "
  echo "     - nfs-server"
  echo "     - nfs-server-uninstall"
  exit 1
}

# INSTALL_K3S_EXEC="--tls-san x.x.x.x"
function k3s_install_master {
  ssh_execute "${HOST_NAME}" "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC=\"--tls-san k3s.oteixido.net --disable=traefik --disable-network-policy\" K3S_NODE_NAME=\"${HOST}\" sh -"
  TOKEN=$(ssh_execute "${HOST_NAME}" "cat /var/lib/rancher/k3s/server/node-token")
  echo -n "${TOKEN}" | pass insert -f -e my/home/k3s/k3s.token
}

function k3s_install_worker {
  K3S_TOKEN=$(pass my/home/k3s/k3s.token)
  ssh_execute "${HOST_NAME}" "curl -sfL https://get.k3s.io | K3S_URL=https://${NODE_MASTER_IP}:6443 K3S_TOKEN=${K3S_TOKEN} K3S_NODE_NAME=\"${NAME}\"  sh -"
}

function k3s_uninstall_master {
  ssh_execute "${HOST_NAME}" "k3s-uninstall.sh"
}

function k3s_uninstall_worker {
  ssh_execute "${HOST_NAME}" "k3s-agent-uninstall.sh"
}

function k3s_node_delete {
  kubectl drain "${HOST_NAME}" --delete-emptydir-data
  kubectl delete node "${HOST_NAME}"
}

function k3s_get_cluster {
  mkdir -p ~/.kube
  ssh_execute "${HOST_NAME}" "cat /etc/rancher/k3s/k3s.yaml" > ~/.kube/config-${NAME}
  sed -i 's/127.0.0.1/k3s.oteixido.net/g' ~/.kube/config-${NAME}
}

function nfs_server {
  ssh_execute "${HOST_NAME}" "mkdir -p /raid1"
  ssh_execute "${HOST_NAME}" "mkdir -p /raid2"
  ssh_execute "${HOST_NAME}" "apt-get install -y nfs-kernel-server"
  ssh_execute "${HOST_NAME}" "truncate -s 0 /etc/exports"
  ssh_execute "${HOST_NAME}" "echo \"/raid1 192.168.10.0/24(rw,no_root_squash,insecure,async,no_subtree_check)\" >> /etc/exports"
  ssh_execute "${HOST_NAME}" "echo \"/raid2 192.168.10.0/24(rw,no_root_squash,insecure,async,no_subtree_check)\" >> /etc/exports"
  ssh_execute "${HOST_NAME}" "echo \"/raid3 192.168.10.0/24(rw,no_root_squash,insecure,async,no_subtree_check)\" >> /etc/exports"
  ssh_execute "${HOST_NAME}" "exportfs -ra"
}

function nfs_server_uninstall {
  ssh_execute "${HOST_NAME}" "truncate -s 0 /etc/exports"
  ssh_execute "${HOST_NAME}" "exportfs -ra"
  ssh_execute "${HOST_NAME}" "umount /raid1 || true"
  ssh_execute "${HOST_NAME}" "umount /raid2 || true"
  ssh_execute "${HOST_NAME}" "umount /raid3 || true"
  ssh_execute "${HOST_NAME}" "mv /etc/fstab /etc/fstab.old && head -5 /etc/fstab.old > /etc/fstab"
}

if [ "$#" -ne "2" ]; then
  show_error
fi

NAME="$2"
HOST_NAME="$2.${NETWORK_DOMAIN}"

case "$1" in
  "k3s-master")
    k3s_install_master
  ;;
  "k3s-master-upgrade")
    k3s_install_master
  ;;
  "k3s-worker")
    k3s_install_worker
  ;;
  "k3s-worker-upgrade")
    k3s_install_worker
  ;;
  "k3s-get-token")
    k3s_get_token
  ;;  
  "k3s-get-cluster")
    k3s_get_cluster
  ;;  
  "k3s-master-uninstall")
    k3s_uninstall_master
  ;;
  "k3s-worker-uninstall")
    k3s_uninstall_worker
  ;;
  "k3s-node-delete")
    k3s_node_delete
  ;;
  "nfs-server")
    node_base
    nfs_server
  ;;
  "nfs-server-uninstall")
    nfs_server_uninstall
  ;;
  *)
    show_error
  ;;
esac

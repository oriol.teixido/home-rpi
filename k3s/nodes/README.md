# Gestió de nodes

# Actualitzar els nodes

En primer lloc actualitzar el node màster.

```bash
pushd ~/source/home/home-rpi/k3s
./k3s uninstall
popd
./config.sh k3s-master-upgrade <MASTER>
./config.sh k3s-worker-upgrade <WORKER1>
./config.sh k3s-worker-upgrade <WORKER2>
pushd ~/source/home/home-rpi/k3s
./k3s install
popd
```

## Moure les aplicacions a un nou cluster

**Parar totes els serveis del cluster actual**

```bash
pushd ~/source/home/home-rpi/k3s/services
./k3s.sh uninstall
popd
```

**Eliminar el(s) node(s) que vulguem del l'actual cluster**

Cal deixar com a mínim el màster per si hi ha cap problema durant la migració dels serveis. A més, cal mantenir el DNS en marxa.

```bash
./config.sh k3s-node-delete $NODE_NAME
./config.sh k3s-worker-uninstall $NODE_NAME
```

**Modificar les variables del fitxer *../cluster***

**Crear un nou cluster**

Crear el node màster i configurar l'entorn per connectar-nos-hi.

```bash
./config.sh k3s-master $NODE_NAME
./config.sh k3s-set-token $NODE_NAME
./config.sh k3s-set-cluster $NODE_NAME $NODE_IP
./config.sh nfs-server $NODE_NAME
```

**Afegir els nodes worker necessaris**

```bash
./config.sh k3s-worker $NODE_NAME
```

**Posar en marxa els serveis bàsics del cluster**

```bash
cd ~/source/home/home-rpi/k3s
./k3s.sh set-labels
cd ~/source/home/home-rpi/k3s/system/ingress
./k3s.sh install
# Esperar que es posin en marxa els pods de l'ingress
cd ~/source/home/home-rpi/k3s/system
./k3s.sh install
```

**Canviar el DNS al router**

WAN > Internet Connection > DNS Server1

**Canviar el disc al nou màster**

Parar el servidor de disc antic i posar el disc al nou servidor. 

```bash
mount -a
```

**Posar en marxa els serveis cluster**

```bash
cd ~/source/home/home-rpi/k3s/services/mysql
./k3s.sh install
# Validar que s'ha posat en marxa el servidor de base de dades
cd ~/source/home/home-rpi/k3s/services
./k3s.sh install
```
#!/bin/bash

source ../../../base/common
source ../../cluster

export BUSYBOX_VERSION=2.6.0

case "$1" in
  install)
    kubectl apply -f persistent-volumes-local.yml
    kubectl apply -f namespace.yml
    kubectl apply -f persistent-volume-claims.yml
    esh secrets.yml.esh | kubectl apply -f -
    esh services.yml.esh | kubectl apply -f -
    esh jobs.yml.esh | kubectl apply -f -
  ;;
  uninstall)
    kubectl delete -f namespace.yml
    kubectl delete -f persistent-volumes-local.yml
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall]"
    exit 1
  ;;
esac

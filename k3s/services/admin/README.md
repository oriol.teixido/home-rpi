# Admin

## Recuperar backup mariadb

Posar en marxa un pod d'administració.

```bash
kubectl scale -n admin --replicas 1 deployment/admin-app
POD=$(kubectl get pods -n admin -l app=admin | tail -1 | awk '{ print $1}')
```

Definir els paràmetres de recuperació

```bash
export PASSWORD_STORE_DIR=~/source/home/home-rpi/password-store
export ROOT_PASSWORD=$(pass my/home/k3s/services/mysql/mysql.root.password | tr -d '\n')
export DATABASE=photoprism
export USERNAME=$(pass my/home/k3s/services/$DATABASE/database.username | tr -d '\n')
export PASSWORD=$(pass my/home/k3s/services/$DATABASE/database.password | tr -d '\n')
export FILENAME=/data/raid1/$DATABASE/backup/20220910230003.gz
```

Recrear la base de dades i restaurar el backup.

```bash
kubectl exec "${POD}" -n admin -- env HOSTNAME="mysql.mysql" DATABASE="$DATABASE" USERNAME="$USERNAME" ROOT_USERNAME="root" ROOT_PASSWORD="$ROOT_PASSWORD" mysql_delete
kubectl exec "${POD}" -n admin -- env HOSTNAME="mysql.mysql" DATABASE="$DATABASE" USERNAME="$USERNAME" PASSWORD="$PASSWORD" ROOT_USERNAME="root" ROOT_PASSWORD="$ROOT_PASSWORD" mysql_create
kubectl exec "${POD}" -n admin -- env HOSTNAME="mysql.mysql" DATABASE="$DATABASE" USERNAME="$USERNAME" PASSWORD="$PASSWORD" mysql_restore "$FILENAME"
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: admin-app
  namespace: admin
  labels:
    app: admin
spec:
  replicas: 0
  selector:
    matchLabels:
      app: admin
  template:
    metadata:
      labels:
        app: admin
    spec:
      containers:
        - name: admin
          image: oteixido/busybox:<%= $BUSYBOX_VERSION %>
          env:
            - name: BORG_CONFIG_DIR
              value: /root/.borg
            - name: DUCKDNS_TOKEN
              valueFrom:
                secretKeyRef:
                  name: admin
                  key: DUCKDNS_TOKEN              
            - name: TELEGRAM_TOKEN
              valueFrom:
                secretKeyRef:
                  name: admin
                  key: TELEGRAM_TOKEN
            - name: TELEGRAM_CHAT_ID
              valueFrom:
                secretKeyRef:
                  name: admin
                  key: TELEGRAM_CHAT_ID
          volumeMounts:
            - name: admin
              subPath: bin
              mountPath: /root/bin
            - name: admin
              subPath: ssh
              mountPath: /root/.ssh
            - name: admin
              subPath: borg
              mountPath: /root/.borg
            - name: raid1
              mountPath: /data/raid1
            - name: raid2
              mountPath: /data/raid2
            - name: tz-config
              mountPath: /etc/localtime
              readOnly: true
      volumes:
        - name: admin
          persistentVolumeClaim:
            claimName: admin
        - name: raid1
          persistentVolumeClaim:
            claimName: raid1
        - name: raid2
          persistentVolumeClaim:
            claimName: raid2
        - name: tz-config
          hostPath:
            path: /usr/share/zoneinfo/Europe/Madrid
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: smtp-to-telegram
  namespace: admin
  labels:
    app: smtp-to-telegram
spec:
  replicas: 1
  selector:
    matchLabels:
      app: smtp-to-telegram
  template:
    metadata:
      labels:
        app: smtp-to-telegram
    spec:
      containers:
        - name: smtp-to-telegram
          image: kostyaesmukov/smtp_to_telegram
          env:
            - name: ST_TELEGRAM_BOT_TOKEN
              valueFrom:
                secretKeyRef:
                  name: admin
                  key: TELEGRAM_TOKEN
            - name: ST_TELEGRAM_CHAT_IDS
              valueFrom:
                secretKeyRef:
                  name: admin
                  key: TELEGRAM_CHAT_ID
          ports:
            - containerPort: 2525
---
apiVersion: v1
kind: Service
metadata:
  name: smtp
  namespace: admin
  labels:
    app: smtp-to-telegram
spec:
  ports:
    - port: 25
      targetPort: 2525
  selector:
    app: smtp-to-telegram

#!/bin/bash

source ../../../base/common
source ../../cluster

case "$1" in
  install)
    kubectl apply -f persistent-volumes.yml
    kubectl apply -f namespace.yml
    kubectl apply -f persistent-volume-claims.yml
    esh services.yml.esh | kubectl apply -f -
    kubectl apply -f ingress.yml
  ;;
  uninstall)
    kubectl delete -f namespace.yml
    kubectl delete -f persistent-volumes.yml
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall]"
    exit 1
  ;;
esac

#!/bin/bash

function install {
  pushd "$1"
  ./k3s.sh install
  popd
}

function uninstall {
  pushd "$1"
  ./k3s.sh uninstall
  popd
}

case "$1" in
  install)
    install mysql
    install influxdb

    install admin
    install nextcloud
    install vaultwarden
    install emby
    install jellyfin
    install firefly
    install domo
    install calibre
    install yii
    install collabora
  ;;
  uninstall)
    uninstall collabora
    uninstall yii
    uninstall calibre
    uninstall domo
    uninstall firefly
    uninstall jellyfin
    uninstall emby
    uninstall vaultwarden
    uninstall nextcloud
    uninstall admin

    uninstall influxdb
    uninstall mysql
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall]"
    exit 1
  ;;
esac

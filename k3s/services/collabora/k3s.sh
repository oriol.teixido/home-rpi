#!/bin/bash

source ../../../base/common
source ../../cluster

export COLLABORA_VERSION=24.04.1.4.1
export COLLABORA_REPLICAS=1

function install {
    kubectl apply -f namespace.yml
    esh secrets.yml.esh | kubectl apply -f -
   esh services.yml.esh | kubectl apply -f -
   kubectl apply -f ingress.yml
}

function uninstall {
    kubectl delete -f namespace.yml
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall]"
    exit 1
  ;;
esac

#!/bin/bash

source ../../../base/common
source ../../cluster

export VAULTWARDEN_VERSION=1.32.2

function install {
    kubectl apply -f persistent-volumes-local.yml
    kubectl apply -f namespace.yml
    kubectl apply -f persistent-volume-claims.yml
    esh secrets.yml.esh | kubectl apply -f -
    esh services.yml.esh | kubectl apply -f -
    kubectl apply -f ingress.yml
}

function uninstall {
    kubectl delete -f namespace.yml
    kubectl delete -f persistent-volumes-local.yml
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall]"
    exit 1
  ;;
esac

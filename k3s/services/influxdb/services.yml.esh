apiVersion: apps/v1
kind: Deployment
metadata:
  name: influxdb
  namespace: influxdb
  labels:
    app: influxdb
spec:
  selector:
    matchLabels:
      app: influxdb
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: influxdb
    spec:
      nodeSelector:
        apps/stable: "true"
      containers:
        - name: influxdb
          image: influxdb:<%= $INFLUXDB_VERSION %>
          env:
            - name: INFLUXDB_ADMIN_USER
              valueFrom:
                secretKeyRef:
                  name: influxdb
                  key: USERNAME
            - name: INFLUXDB_ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: influxdb
                  key: PASSWORD
          ports:
            - containerPort: 8086
          volumeMounts:
            - name: data
              subPath: data
              mountPath: /var/lib/influxdb2
            - name: tz-config
              mountPath: /etc/localtime
              readOnly: true
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: data
        - name: tz-config
          hostPath:
            path: /usr/share/zoneinfo/Europe/Madrid
---
apiVersion: v1
kind: Service
metadata:
  name: influxdb
  namespace: influxdb
  labels:
    app: influxdb
spec:
  ports:
    - port: 8086
      targetPort: 8086
  selector:
    app: influxdb
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nextcloud
  namespace: nextcloud
  labels:
    app: nextcloud
spec:
  replicas: <%= $NEXTCLOUD_REPLICAS %>
  selector:
    matchLabels:
      app: nextcloud
  template:
    metadata:
      labels:
        app: nextcloud
    spec:
      containers:
        - name: nextcloud
          image: oteixido/nextcloud:<%= $NEXTCLOUD_VERSION %>
          env:
            - name: REDIS_HOST
              value: redis
            - name: REDIS_HOST_PASSWORD
              value: default-password
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: "512Mi"
            limits:
              memory: "1Gi"
          volumeMounts:
            - name: nextcloud-files
              subPath: nc
              mountPath: /var/www/html
            - name: tz-config
              mountPath: /etc/localtime
              readOnly: true
      volumes:
        - name: nextcloud-files
          persistentVolumeClaim:
            claimName: nextcloud-files
        - name: tz-config
          hostPath:
            path: /usr/share/zoneinfo/Europe/Madrid
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
  namespace: nextcloud
  labels:
    app: redis
spec:
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
        - name: redis
          image: redis:6.0.9-alpine
          args: ["--bind", "0.0.0.0", "--requirepass", "default-password"]
          ports:
            - containerPort: 6379
          resources:
            requests:
              memory: "16Mi"
            limits:
              memory: "64Mi"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: go-vod
  namespace: nextcloud
  labels:
    app: go-vod
spec:
  selector:
    matchLabels:
      app: go-vod
  template:
    metadata:
      labels:
        app: go-vod
    spec:
      containers:
        - name: go-vod
          image: radialapps/go-vod
          ports:
            - containerPort: 47788
          env:
            - name: NEXTCLOUD_HOST
              value: https://cloud.oteixido.net
          securityContext:
            privileged: true
          resources:
            requests:
              memory: "256Mi"
            limits:
              memory: "512Mi"
          volumeMounts:
            - name: nextcloud-files
              subPath: nc
              mountPath: /var/www/html
              readOnly: true
            - name: tz-config
              mountPath: /etc/localtime
              readOnly: true
            - name: renderd128
              mountPath: /dev/dri/renderD128
      volumes:
        - name: nextcloud-files
          persistentVolumeClaim:
            claimName: nextcloud-files
        - name: tz-config
          hostPath:
            path: /usr/share/zoneinfo/Europe/Madrid
        - name: renderd128
          hostPath:
            path: /dev/dri/renderD128
---
apiVersion: v1
kind: Service
metadata:
  name: go-vod
  namespace: nextcloud
  labels:
    app: go-vod
spec:
  ports:
    - port: 47788
      targetPort: 47788
  selector:
    app: go-vod
---
apiVersion: v1
kind: Service
metadata:
  name: nextcloud
  namespace: nextcloud
  labels:
    app: nextcloud
spec:
  ports:
    - port: 80
      targetPort: 80
  selector:
    app: nextcloud
---
apiVersion: v1
kind: Service
metadata:
  name: redis
  namespace: nextcloud
  labels:
    app: redis
spec:
  ports:
    - port: 6379
      targetPort: 6379
  selector:
    app: redis

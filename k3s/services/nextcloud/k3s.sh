#!/bin/bash

source ../../../base/common
source ../../cluster

export NEXTCLOUD_VERSION=29.0.11
export NEXTCLOUD_REPLICAS=2

function install {
    kubectl apply -f persistent-volumes-nfs.yml
    kubectl apply -f namespace.yml
    kubectl apply -f persistent-volume-claims.yml
    esh secrets.yml.esh | kubectl apply -f -
    esh services.yml.esh | kubectl apply -f -
    kubectl apply -f ingress.yml
    esh jobs.yml.esh | kubectl apply -f -
}

function uninstall {
    kubectl delete -f namespace.yml
    kubectl delete -f persistent-volumes-nfs.yml
}

function fix {
    POD=$(kubectl get pods -n nextcloud -l app=nextcloud | tail -1 | awk '{ print $1}')
    MYSQL=$(kubectl get pods -n mysql -l app=mysql | tail -1 | awk '{ print $1}')

    kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ files:scan --all"
    kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ files:cleanup"

    kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ maintenance:mode --on"
    echo "DELETE FROM oc_file_locks WHERE 1;" | kubectl exec -i "${MYSQL}" -n mysql -- mysql -u $(pass my/home/k3s/services/nextcloud/database.username) -p$(pass my/home/k3s/services/nextcloud/database.password) nextcloud
    kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ maintenance:mode --off"
}

function cleanup-trash {
    POD=$(kubectl get pods -n nextcloud -l app=nextcloud | tail -1 | awk '{ print $1}')
    kubectl exec -it "${POD}" -n nextcloud  -- su www-data -s /bin/sh -c "./occ trashbin:cleanup --all-users"
    kubectl exec -it "${POD}" -n nextcloud  -- su www-data -s /bin/sh -c "./occ groupfolders:trashbin:cleanup" 
}

function cleanup-versions {
    POD=$(kubectl get pods -n nextcloud -l app=nextcloud | tail -1 | awk '{ print $1}')
    kubectl exec -it "${POD}" -n nextcloud  -- su www-data -s /bin/sh -c "./occ versions:cleanup"
}

function wait-pod {
  LABELS=$1
  STATUS=$2
  POD=$(kubectl get pods -n nextcloud ${LABELS} --ignore-not-found | tail -n +2 | grep "${STATUS}" | awk '{ print $1}')
  while [ -z "${POD}" ]; do
    sleep 5
    POD=$(kubectl get pods -n nextcloud ${LABELS} --ignore-not-found | tail -n +2 | grep "${STATUS}" | awk '{ print $1}')
  done
  echo ${POD} | awk '{print $1}'
}

function wait-not-pod {
  LABELS=$1
  STATUS=$2
  POD=$(kubectl get pods -n nextcloud ${LABELS} --ignore-not-found | tail -n +2 | grep "${STATUS}" | awk '{ print $1}')
  while [ ! -z "${POD}" ]; do
    sleep 5
    POD=$(kubectl get pods -n nextcloud ${LABELS} --ignore-not-found | tail -n +2 | grep "${STATUS}" | awk '{ print $1}')
  done
  echo ${POD} | awk '{print $1}'
}

function upgrade1 {
  echo "Deleting ingress..."
  kubectl delete -f ingress.yml --ignore-not-found > /dev/null
  echo "Deleting services and wait all pods..."
  esh services.yml.esh | kubectl delete -f - --ignore-not-found > /dev/null

  echo "Suspending all cronjobs..."
  kubectl patch cronjobs nextcloud-backup-db -n nextcloud -p '{"spec" : {"suspend" : true }}'  > /dev/null
  kubectl patch cronjobs nextcloud-backup-files -n nextcloud -p '{"spec" : {"suspend" : true }}' > /dev/null
  kubectl patch cronjobs nextcloud-cron -n nextcloud -p '{"spec" : {"suspend" : true }}' > /dev/null
  kubectl patch cronjobs nextcloud-preview -n nextcloud -p '{"spec" : {"suspend" : true }}' > /dev/null

  kubectl delete pods --field-selector status.phase=Failed -n nextcloud --ignore-not-found > /dev/null
  kubectl delete pods --field-selector status.phase=Succeeded -n nextcloud --ignore-not-found > /dev/null
  kubectl wait pod --for delete -l app=nextcloud

  echo "Creating backup jobs..."
  kubectl create job -n nextcloud --from=cronjob/nextcloud-backup-db nextcloud-backup-db-upgrade > /dev/null
  kubectl create job -n nextcloud --from=cronjob/nextcloud-backup-files nextcloud-backup-files-upgrade > /dev/null

  echo "Waiting db backup..."
  kubectl wait pods -l app=nextcloud-backup-db --for condition=Ready --timeout=5m > /dev/null
  kubectl wait pods -l app=nextcloud-backup-db --for condition=Ready=False --timeout=30m > /dev/null
  kubectl delete job -n nextcloud nextcloud-backup-db-upgrade > /dev/null

  echo "Waiting files backup..."
  kubectl wait pods -l app=nextcloud-backup-files --for condition=Ready --timeout=5m > /dev/null
  kubectl wait pods -l app=nextcloud-backup-files --for condition=Ready=False --timeout=30m > /dev/null
  kubectl delete job -n nextcloud nextcloud-backup-files-upgrade > /dev/null

  echo "Creating new pod for upgrade to ${NEXTCLOUD_VERSION}..."
  NEXTCLOUD_REPLICAS=1 esh services.yml.esh | kubectl apply -f - >/dev/null
  kubectl wait pods -l app=nextcloud --for condition=Ready --timeout=5m > /dev/null
  POD=$(kubectl get pods -n nextcloud -l app=nextcloud | tail -n +2 | awk '{ print $1}')
  kubectl logs -f -n nextcloud "${POD}"
}

function upgrade2 {
  POD=$(kubectl get pods -n nextcloud -l app=nextcloud | tail -n +2 | awk '{ print $1}')
  echo "Executing upgrade to ${NEXTCLOUD_VERSION}..."
  kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ upgrade"
  kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ maintenance:repair --include-expensive"
  kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ maintenance:mode --off"

  echo "Creating all services..."
  esh services.yml.esh | kubectl apply -f - >/dev/null

  echo "Adding misssing indices to ${NEXTCLOUD_VERSION}..."
  kubectl exec "${POD}" -n nextcloud -- su www-data -s /bin/bash -c "./occ db:add-missing-indices"

  echo "Recreating all cronjobs..."
  esh jobs.yml.esh | kubectl delete -f - >/dev/null
  esh jobs.yml.esh | kubectl apply -f - >/dev/null

  echo "Creating all ingress"
  kubectl apply -f ingress.yml >/dev/null

  echo "DONE"
}

function docker-build {
    docker buildx build --push --platform linux/amd64,linux/arm64 --build-arg="NEXTCLOUD_VERSION=${NEXTCLOUD_VERSION}" --tag "oteixido/nextcloud:${NEXTCLOUD_VERSION}" .
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;
  upgrade1)
    docker-build
    upgrade1
  ;;
  upgrade2)
    upgrade2
  ;;
  close)
    close
  ;;
  fix)
    fix
  ;;
  cleanup-trash)
    cleanup-trash
  ;;
  cleanup-versions)
    cleanup-versions
  ;;
  docker-build)
    docker-build
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall|upgrade|close|fix|cleanup-trash|cleanup-versions|docker-build]"
    exit 1
  ;;
esac

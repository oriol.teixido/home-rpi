# Nextcloud

## Paràmetres de configuració

```console
$ su www-data -s /bin/bash -c "./occ config:app:set --value='64 256 512' previewgenerator squareSizes"
$ su www-data -s /bin/bash -c "./occ config:app:set --value='64 256 512' previewgenerator widthSizes"
$ su www-data -s /bin/bash -c "./occ config:app:set --value='64 256 512' previewgenerator heightSizes"
```


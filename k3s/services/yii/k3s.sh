#!/bin/bash

source ../../../base/common
source ../../cluster

function install {
  kubectl apply -f persistent-volumes-nfs.yml
  kubectl apply -f namespace.yml
  kubectl apply -f persistent-volume-claims.yml
  esh secrets.yml.esh | kubectl apply -f -
  kubectl apply -f services.yml
  kubectl apply -f ingress.yml
  kubectl apply -f jobs.yml
}

function uninstall {
  kubectl delete -f namespace.yml
  kubectl delete -f persistent-volumes-nfs.yml
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall]"
    exit 1
  ;;
esac

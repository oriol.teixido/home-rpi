#!/bin/bash

source ../../../base/common
source ../../cluster

export FIREFLY_VERSION=version-6.1.16
export IMPORTER_VERSION=version-1.5.2
export FIREFLY_REPLICAS=1

function install {
    kubectl apply -f persistent-volumes-nfs.yml
    kubectl apply -f namespace.yml
    kubectl apply -f persistent-volume-claims.yml
    esh secrets.yml.esh | kubectl apply -f -
    esh services.yml.esh | kubectl apply -f -
    kubectl apply -f ingress.yml
    esh jobs.yml.esh | kubectl apply -f -
}

function uninstall {
    kubectl delete -f namespace.yml
    kubectl delete -f persistent-volumes-nfs.yml
}

case "$1" in
  install)
    install
  ;;
  uninstall)
    uninstall
  ;;
  reinstall)
    uninstall
    install
  ;;
  *)
    echo "ERROR. Invalid syntax."
    echo "  $0 [install|uninstall|reinstall]"
    exit 1
  ;;
esac

USE firefly;
SET @amount=2000;
SET @data_inici='2023-06-01';
SET @data_final='2023-06-30';
SET @category=NULL;
SELECT transaction_journals.description, transaction_journals.date, transactions.amount, categories.name
FROM transactions, transaction_journals, accounts, category_transaction_journal, categories
WHERE transactions.transaction_journal_id = transaction_journals.id
  AND transactions.account_id = accounts.id
  AND transaction_journals.id = category_transaction_journal.transaction_journal_id
  AND category_transaction_journal.category_id = categories.id
  AND (category_transaction_journal.category_id = @category OR @category IS NULL)
  AND accounts.account_type_id IN (3,12)
  AND transactions.deleted_at IS NULL
  AND transaction_journals.date BETWEEN @data_inici AND @data_final
  AND (transactions.amount > @amount OR transactions.amount < -@amount);
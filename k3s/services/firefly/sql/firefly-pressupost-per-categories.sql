USE firefly;
SET @inici='2024-01-01';
SET @final='2024-12-31';
SELECT c.name, COALESCE(SUM(t.amount), 0)
FROM (SELECT categories.id, categories.name
      FROM categories
      WHERE name NOT LIKE 'moto:%' AND name NOT IN ('Transferencia') AND deleted_at IS NULL) AS c
         LEFT JOIN
     (SELECT category_transaction_journal.category_id, transactions.amount
      FROM transactions, transaction_journals, category_transaction_journal
      WHERE transaction_journals.id = transactions.transaction_journal_id
        AND transaction_journals.id = category_transaction_journal.transaction_journal_id
        AND transactions.account_id NOT IN (6, 7)
        AND transactions.deleted_at IS NULL
        AND transaction_journals.deleted_at IS NULL
        AND transaction_journals.date between @inici AND @final) AS t
     ON c.id = t.category_id
GROUP BY c.name;
USE firefly;
SELECT accounts.name, SUM(transactions.amount)
FROM transactions, accounts
WHERE transactions.account_id = accounts.id
  AND transactions.deleted_at IS NULL
  AND accounts.deleted_at IS NULL
  AND accounts.active = 1
  AND accounts.account_type_id IN (3,12)
GROUP BY transactions.account_id
ORDER BY NAME;
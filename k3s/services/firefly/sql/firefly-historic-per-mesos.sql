USE firefly;
SELECT CONCAT(YEAR(transaction_journals.date), '-', LPAD(MONTH(transaction_journals.date), 2, 0)) as YEAR, SUM(transactions.amount)
FROM transaction_journals, transactions, accounts
WHERE transactions.transaction_journal_id = transaction_journals.id
  AND transactions.account_id = accounts.id
  AND accounts.account_type_id IN (3,12)
  AND transactions.deleted_at IS NULL GROUP BY YEAR ORDER BY 1;
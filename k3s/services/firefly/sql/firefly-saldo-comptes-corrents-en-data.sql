USE firefly;
SELECT accounts.name, accounts.id, SUM(transactions.amount)
FROM transactions, accounts, transaction_journals
WHERE transactions.account_id = accounts.id
  AND transactions.deleted_at IS NULL
  AND accounts.id = 24
  AND transactions.transaction_journal_id = transaction_journals.id
  AND transaction_journals.date < '2024-07-15'
GROUP BY transactions.account_id
ORDER BY NAME;